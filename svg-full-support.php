<?php
/*
Plugin Name: SVG full support
Plugin URI: http://www.wearewp.pro/
Description: Full support of SVG format in WordPress
Author: WeAreWP
Author URI: http://www.wearewp.pro
Text Domain: svg-fs
Domain Path: /languages/
Version: 1.0
Date: 28/12/2016
Source: https://www.sitepoint.com/wordpress-svg/
*/

define('SVGFS_DIR', plugin_dir_path(__FILE__));
define('SVGFS_URL', plugins_url('/', __FILE__));


class SVG_Full_Support
{
    private static $instance = null;

    public function __construct()
    {
        add_action('init', array($this, 'load_textdomain'), 0);

        add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'), 999);
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('upload_mimes', array($this, 'add_file_types_to_uploads'));

        add_action('wp_ajax_svg_get_attachment_url', array($this, 'get_attachment_url_media_library'));
    }

    public function load_textdomain()
    {
        load_plugin_textdomain('svg-fs', false, basename(dirname(__FILE__)) . '/languages');
    }

    public function admin_enqueue_scripts($hook)
    {
        wp_enqueue_style('admin-svgfs', SVGFS_URL . 'css/admin.css');
        wp_enqueue_script('admin-svgfs', SVGFS_URL . 'js/admin.js', array('jquery'));
    }

    public function enqueue_scripts($hook)
    {
        wp_enqueue_style('admin-svgfs', SVGFS_URL . 'css/sgv-fs.css');
    }

    //add SVG to allowed file uploads
    public function add_file_types_to_uploads($file_types)
    {

        $new_filetypes = array();
        $new_filetypes['svg'] = 'image/svg+xml';
        $file_types = array_merge($file_types, $new_filetypes);

        return $file_types;
    }

    //called via AJAX. returns the full URL of a media attachment (SVG)
    public function get_attachment_url_media_library()
    {

        $url = '';
        $attachmentID = isset($_REQUEST['attachmentID']) ? $_REQUEST['attachmentID'] : '';
        if ($attachmentID) {
            $url = wp_get_attachment_url($attachmentID);
        }

        echo $url;

        die();
    }

    //set / return singleton
    public static function getInstance(){
        if(is_null(self::$instance)){
            self::$instance = new self();
        }
        return self::$instance;
    }
}

$svg_full_support = SVG_Full_Support::getInstance();